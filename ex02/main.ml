let () =
	let cAS = Card.newCard Card.Value.As Card.Color.Spade in
	let cKS = Card.newCard Card.Value.King Card.Color.Spade in
	let cQS = Card.newCard Card.Value.Queen Card.Color.Spade in
	let cJS = Card.newCard Card.Value.Jack Card.Color.Spade in
	let c10S = Card.newCard Card.Value.T10 Card.Color.Spade in
	let c9S = Card.newCard Card.Value.T9 Card.Color.Spade in
	let c8S = Card.newCard Card.Value.T8 Card.Color.Spade in
	let c7S = Card.newCard Card.Value.T7 Card.Color.Spade in
	let c6S = Card.newCard Card.Value.T6 Card.Color.Spade in
	let c5S = Card.newCard Card.Value.T5 Card.Color.Spade in
	let c4S = Card.newCard Card.Value.T4 Card.Color.Spade in
	let c3S = Card.newCard Card.Value.T3 Card.Color.Spade in
	let c2S = Card.newCard Card.Value.T2 Card.Color.Spade in

	let cAH = Card.newCard Card.Value.As Card.Color.Heart in
	let cKH = Card.newCard Card.Value.King Card.Color.Heart in
	let cQH = Card.newCard Card.Value.Queen Card.Color.Heart in
	let cJH = Card.newCard Card.Value.Jack Card.Color.Heart in
	let c10H = Card.newCard Card.Value.T10 Card.Color.Heart in
	let c9H = Card.newCard Card.Value.T9 Card.Color.Heart in
	let c8H = Card.newCard Card.Value.T8 Card.Color.Heart in
	let c7H = Card.newCard Card.Value.T7 Card.Color.Heart in
	let c6H = Card.newCard Card.Value.T6 Card.Color.Heart in
	let c5H = Card.newCard Card.Value.T5 Card.Color.Heart in
	let c4H = Card.newCard Card.Value.T4 Card.Color.Heart in
	let c3H = Card.newCard Card.Value.T3 Card.Color.Heart in
	let c2H = Card.newCard Card.Value.T2 Card.Color.Heart in

	let cAD = Card.newCard Card.Value.As Card.Color.Diamond in
	let cKD = Card.newCard Card.Value.King Card.Color.Diamond in
	let cQD = Card.newCard Card.Value.Queen Card.Color.Diamond in
	let cJD = Card.newCard Card.Value.Jack Card.Color.Diamond in
	let c10D = Card.newCard Card.Value.T10 Card.Color.Diamond in
	let c9D = Card.newCard Card.Value.T9 Card.Color.Diamond in
	let c8D = Card.newCard Card.Value.T8 Card.Color.Diamond in
	let c7D = Card.newCard Card.Value.T7 Card.Color.Diamond in
	let c6D = Card.newCard Card.Value.T6 Card.Color.Diamond in
	let c5D = Card.newCard Card.Value.T5 Card.Color.Diamond in
	let c4D = Card.newCard Card.Value.T4 Card.Color.Diamond in
	let c3D = Card.newCard Card.Value.T3 Card.Color.Diamond in
	let c2D = Card.newCard Card.Value.T2 Card.Color.Diamond in

	let cAC = Card.newCard Card.Value.As Card.Color.Club in
	let cKC = Card.newCard Card.Value.King Card.Color.Club in
	let cQC = Card.newCard Card.Value.Queen Card.Color.Club in
	let cJC = Card.newCard Card.Value.Jack Card.Color.Club in
	let c10C = Card.newCard Card.Value.T10 Card.Color.Club in
	let c9C = Card.newCard Card.Value.T9 Card.Color.Club in
	let c8C = Card.newCard Card.Value.T8 Card.Color.Club in
	let c7C = Card.newCard Card.Value.T7 Card.Color.Club in
	let c6C = Card.newCard Card.Value.T6 Card.Color.Club in
	let c5C = Card.newCard Card.Value.T5 Card.Color.Club in
	let c4C = Card.newCard Card.Value.T4 Card.Color.Club in
	let c3C = Card.newCard Card.Value.T3 Card.Color.Club in
	let c2C = Card.newCard Card.Value.T2 Card.Color.Club in

	let rec toStringAll ft l = match l with
		| []			-> print_string ""
		| head::tail 	-> 
			begin
				print_endline (ft head);
				toStringAll ft tail
			end
	in
	print_endline "------------  max/min/compare --------------------";
	print_endline (Card.toString (Card.max cAS cAS));  
	print_endline (Card.toString (Card.max c2C c8D));  
	print_endline (Card.toString (Card.max cAS cJC));
	print_newline ();  
	print_endline (Card.toString (Card.min cAC cKC));  
	print_endline (Card.toString (Card.min c2C c8D));  
	print_endline (Card.toString (Card.min cAC cKC));  
	print_newline ();  
	print_int (Card.compare cAS cAS); print_newline (); 
	print_int (Card.compare c2C c8D); print_newline (); 
	print_int (Card.compare cAC cKC); print_newline (); 
	print_endline "-------------------- toInt (getValue ) -----------"; 
	print_int (Card.Value.toInt (Card.getValue cAC)); print_newline (); 
	print_int (Card.Value.toInt (Card.getValue c2C)); print_newline (); 
	print_int (Card.Value.toInt (Card.getValue c8C)); print_newline (); 
	print_int (Card.Value.toInt (Card.getValue cKC)); print_newline (); 
	print_int (Card.Value.toInt (Card.getValue cJC)); print_newline (); 
	print_endline "-------------------- toStringVerbose (getColor ) -----------"; 
	print_string (Card.Color.toStringVerbose (Card.getColor cAC)); print_newline (); 
	print_string (Card.Color.toStringVerbose (Card.getColor c2C)); print_newline (); 
	print_string (Card.Color.toStringVerbose (Card.getColor c8D)); print_newline (); 
	print_string (Card.Color.toStringVerbose (Card.getColor cKH)); print_newline (); 
	print_string (Card.Color.toStringVerbose (Card.getColor cJS)); print_newline (); 


	print_endline "------------  best --------------------";
	let b_1 = Card.best Card.all in
	print_endline (Card.toString b_1);
	let b_2 =  Card.best [c9H;c8H;c7H;c7C;c6H;c5H;c4H;c3H;c2H] in 
	print_endline (Card.toString b_2);
	(* Card.best []  (* raise a error *)*) 
	print_endline "----------------  all Spades ------------------------";
	toStringAll (Card.toString) Card.allSpades;
	print_endline "----------------  all Clubs ------------------------";
	toStringAll (Card.toString) Card.allClubs;
	print_endline "----------------  all Diamonds ------------------------";
	toStringAll (Card.toString) Card.allDiamonds;
	print_endline "----------------  all Hearts ------------------------";
	toStringAll (Card.toString) Card.allHearts;
	print_endline "----------------  all ------------------------";
	toStringAll (Card.toString) Card.all;
	print_endline "----------------  all Spades Verbose ------------------------";
	toStringAll (Card.toStringVerbose) Card.allSpades;
	print_endline "----------------  all Clubs Verbose ------------------------";
	toStringAll (Card.toStringVerbose) Card.allClubs;
	print_endline "----------------  all Diamonds Verbose ------------------------";
	toStringAll (Card.toStringVerbose) Card.allDiamonds;
	print_endline "----------------  all Hearts Verbose ------------------------";
	toStringAll (Card.toStringVerbose) Card.allHearts;
	print_endline "----------------  all Verbose ------------------------";
	toStringAll (Card.toStringVerbose) Card.all;
