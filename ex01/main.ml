
let () = 
	let cJ = Value.Jack in
	let ca = Value.As in
	print_endline "===== int =====" ;
	List.iter (fun i -> Printf.printf "%d\n" (Value.toInt i)) Value.all;
	print_newline ();
	print_endline "===== string =====" ;
	List.iter (fun i -> Printf.printf "%s\n" (Value.toString i)) Value.all;
	print_newline ();
	print_endline "===== verbose =====" ;
	List.iter (fun i -> Printf.printf "%s\n" (Value.toStringVerbose i)) Value.all;
	print_newline ();
	print_endline "===== misc =====" ;
	Printf.printf "Jack : %d\n" (Value.toInt cJ);
	Printf.printf "As previous : %s\n" (Value.toString (Value.previous ca));
	Printf.printf "As next : %s\n" (Value.toStringVerbose (Value.next ca));
