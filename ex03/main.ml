let () =
	let cAS = Deck.Card.newCard Deck.Card.Value.As Deck.Card.Color.Spade in

	let c9H = Deck.Card.newCard Deck.Card.Value.T9 Deck.Card.Color.Heart in
	let c8H = Deck.Card.newCard Deck.Card.Value.T8 Deck.Card.Color.Heart in
	let c7H = Deck.Card.newCard Deck.Card.Value.T7 Deck.Card.Color.Heart in
	let c6H = Deck.Card.newCard Deck.Card.Value.T6 Deck.Card.Color.Heart in
	let c5H = Deck.Card.newCard Deck.Card.Value.T5 Deck.Card.Color.Heart in
	let c4H = Deck.Card.newCard Deck.Card.Value.T4 Deck.Card.Color.Heart in
	let c3H = Deck.Card.newCard Deck.Card.Value.T3 Deck.Card.Color.Heart in
	let c2H = Deck.Card.newCard Deck.Card.Value.T2 Deck.Card.Color.Heart in

	let cAC = Deck.Card.newCard Deck.Card.Value.As Deck.Card.Color.Club in
	let cKC = Deck.Card.newCard Deck.Card.Value.King Deck.Card.Color.Club in
	let c10C = Deck.Card.newCard Deck.Card.Value.T10 Deck.Card.Color.Club in
	let c7C = Deck.Card.newCard Deck.Card.Value.T7 Deck.Card.Color.Club in

	let c8D = Deck.Card.newCard Deck.Card.Value.T8 Deck.Card.Color.Diamond in

	print_endline "------------  max/min/compare --------------------";
	print_endline (Deck.Card.toString (Deck.Card.max cAS cAS));  print_newline ();  
	print_endline (Deck.Card.toString (Deck.Card.min cAC cKC));  print_newline ();  
	print_int (Deck.Card.compare cAS c8D); print_newline (); 
	print_endline "-------------------- toInt (getValue ) -----------"; 
	print_int (Deck.Card.Value.toInt (Deck.Card.getValue c10C)); print_newline (); 
	print_endline "-------------------- toStringVerbose (getColor ) -----------"; 
	print_string (Deck.Card.Color.toStringVerbose (Deck.Card.getColor c4H)); print_newline (); 
	print_endline "------------  best --------------------";
let b_1 = Deck.Card.best Deck.Card.all in
	print_endline (Deck.Card.toString b_1);
let b_2 =  Deck.Card.best [c9H;c8H;c7H;c7C;c6H;c5H;c4H;c3H;c2H] in 
	print_endline (Deck.Card.toString b_2);
	try
		 ignore(Deck.Card.best [])  (* raise a error *) 
	with
	| invalid_arg -> print_endline "invalid_arg : no cards";
let rec toStringAll ft l = match l with
	| []			-> print_string ""
	| head::tail 	-> 
		begin
			print_endline (ft head);
			toStringAll ft tail
		end
in
	print_endline "----------------  all Spades ------------------------";
	toStringAll (Deck.Card.toString) Deck.Card.allSpades;
	print_endline "----------------  all Clubs ------------------------";
	toStringAll (Deck.Card.toString) Deck.Card.allClubs;
	print_endline "----------------  all Diamonds ------------------------";
	toStringAll (Deck.Card.toString) Deck.Card.allDiamonds;
	print_endline "----------------  all Hearts ------------------------";
	toStringAll (Deck.Card.toString) Deck.Card.allHearts;
	print_endline "----------------  all ------------------------";
	toStringAll (Deck.Card.toString) Deck.Card.all;
	print_endline "----------------  all Spades Verbose ------------------------";
	toStringAll (Deck.Card.toStringVerbose) Deck.Card.allSpades;
	print_endline "----------------  all Clubs Verbose ------------------------";
	toStringAll (Deck.Card.toStringVerbose) Deck.Card.allClubs;
	print_endline "----------------  all Diamonds Verbose ------------------------";
	toStringAll (Deck.Card.toStringVerbose) Deck.Card.allDiamonds;
	print_endline "----------------  all Hearts Verbose ------------------------";
	toStringAll (Deck.Card.toStringVerbose) Deck.Card.allHearts;
	print_endline "----------------  all Verbose ------------------------";
	toStringAll (Deck.Card.toStringVerbose) Deck.Card.all;
let deck1 = Deck.newDeck () in
let deck2 = Deck.newDeck () in
let print_deck d =
	List.iter (fun d -> Printf.printf "%s " d) (Deck.toStringList d) ; print_newline ()
in
let print_deck_verbose d =
	List.iter (fun d -> Printf.printf "%s\n" d) (Deck.toStringListVerbose d) ; print_newline ()
in
let show_card (c, d) =
	Printf.printf "card draw : %s" (Deck.Card.toString c);
	print_endline "\nremaining in deck : ";
	print_deck d
in
	print_endline "=====   toStringList   =====";
	print_deck deck1;
	print_endline "=====   toStringListVerbose   =====";
	print_deck_verbose deck2;
	print_endline "=====   drawCard   =====";
	try
		while true do
			let rec loop (c, d) =
				loop (Deck.drawCard d)
			in loop (Deck.drawCard deck1)
		done
	with
	| failwith ->  print_endline "failwith: deck is empty" ;
	show_card (Deck.drawCard deck1);
